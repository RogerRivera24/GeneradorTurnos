﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace GeneradorTurnos {
    public partial class frmGenerador : Form {        
        List<int> listaGrupos = new List<int>();
        
        public frmGenerador() {
            InitializeComponent();     
            Enumerable.Range(1, 17).Where(x => x != 9).ToList().ForEach(x => {
                listaGrupos.Add(x);
            });
        }

        private void btnIniciar_Click(object sender, EventArgs e) {
            btnIniciar.Enabled = false;
            if (listaGrupos.Count() > 0) {
                ((Label)this.Controls["panelTurnos"]
                            .Controls["lblNumero" + generarNumero()
                            .ToString()])
                            .ForeColor = Color.Blue;        
            }
            else {
                MessageBox.Show("Terminó la clase", "Fin de presentaciones", MessageBoxButtons.OK);
            }
            btnIniciar.Enabled = true;
        }

        private int generarNumero() {
            int numeroElegido = 0; 

            Enumerable.Range(1, 60).ToList().ForEach(x => {
                numeroElegido = listaGrupos.ElementAt(new Random().Next(0, listaGrupos.Count));
                lblNumeroElegido.Text = numeroElegido.ToString();
                Application.DoEvents();
                Thread.Sleep(10);
            });
           
            listaGrupos.Remove(numeroElegido);
            return numeroElegido;
        }
    }
}
